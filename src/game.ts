/////////////////////////////////////////////////////
// This UI shows a source image mapping bug.
// Carl Fravel
// Note that the upper UIImage correctly shows just the selected portion of the source image,
// whereas the lower UIImage shows the entire source image, not just the selected portion.
/////////////////////////////////////////////////////

class DemoUI {
    canvas:UICanvas = null
    uiContainer: UIContainerRect
    badUiImage:UIImage
    goodUiImage:UIImage

    constructor () {
        this.setupUI()
    }

    setupUI (){
        const hudBackgroundColor = Color4.White()
        // load the image atlases
        let badUiAtlas = "materials/ui-images/bad-image.png"
        let goodUiAtlas = "materials/ui-images/good-image.png"
        let badUiTexture = new Texture(badUiAtlas)
        let goodUiTexture = new Texture(goodUiAtlas)

        // Create canvas component
        this.canvas = new UICanvas()
        this.canvas.hAlign = 'center'
        this.canvas.vAlign = 'bottom'
        
        // Container       
        this.uiContainer = new UIContainerRect(this.canvas)
        this.uiContainer.hAlign = 'right'
        this.uiContainer.vAlign = 'bottom'
        this.uiContainer.width = 140
        this.uiContainer.height = 145
        this.uiContainer.positionX = 0
        this.uiContainer.positionY = 200
        this.uiContainer.color = hudBackgroundColor

        // Demonstrates correct source region selectoin failure
        this.goodUiImage = new UIImage(this.uiContainer, goodUiTexture)
        this.goodUiImage.sourceLeft = 0
        this.goodUiImage.sourceTop = 0
        this.goodUiImage.sourceWidth = 512
        this.goodUiImage.sourceHeight = 256
        this.goodUiImage.hAlign = 'left'
        this.goodUiImage.vAlign = 'top'
        this.goodUiImage.positionX = 5
        this.goodUiImage.positionY = -5
        this.goodUiImage.width=128
        this.goodUiImage.height=64
        this.goodUiImage.isPointerBlocker = true

        // Demonstrates source region selection failure
        this.badUiImage = new UIImage(this.uiContainer, badUiTexture)
        this.badUiImage.sourceLeft = 0
        this.badUiImage.sourceTop = 0
        this.badUiImage.sourceWidth = 512
        this.badUiImage.sourceHeight = 256
        this.badUiImage.hAlign = 'left'
        this.badUiImage.vAlign = 'top'
        this.badUiImage.positionX = 5
        this.badUiImage.positionY = -74
        this.badUiImage.width=128
        this.badUiImage.height=64
        this.badUiImage.isPointerBlocker = true

        this.uiContainer.visible = true
    }
}

let demoUI = new DemoUI()

