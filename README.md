This Scene has a UI that shows a source image mapping bug.

The issue seems to be something about the handling of the "bad" .png file

Note that the upper UIImage correctly shows just the selected portion of the "good" source image, whereas the lower UIImage shows the entire "bad" source image, not just the selected portion.


Carl Fravel
